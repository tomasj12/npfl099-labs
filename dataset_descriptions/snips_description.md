# Natural Language Understanding benchmark

### What kind of data it is ?
- domain: multidomain
- modality: text
- the data collected in this dataset are used for benchmarking common personal assistents (e.g. API.ai, Wit.ai, Luis.ai, Alexa, Snips)
- the dataset is divided into 7 parts by intents (SearchCreativeWork, GetWeather, BookRestaurant, PlayMusic, AddToPlaylist, RateBook, SearchScreeningEvent)
- only textual, annotated data is present
- only queries for the assistents with annotations for evaluation
- therefore no distinction user/system, no dialogues, no turns are present

### How was it collected ?
- by crowdsourcing

### What kind of dialogue system / dialogue system component is it designed for ?
- the dataset is designed for benchmarking the language model and the natural language understanding part of the personal assistents and it focuses mainly on _slot filling_, therefore every annotation annotates which slot (if any) can be filled with the corresponding word

### What kind of annotation is present ?
- every sentence is stored in the same form :
```javascript
"data": [
        {
          "text": "Add another "
        },
        {
          "text": "song",
          "entity": "music_item"
        },
        {
          "text": " to the "
        },
        {
          "text": "Cita Romántica",
          "entity": "playlist"
        },
        {
          "text": " playlist. "
        }
      ]
```
- so annotation shows which entity the corresponding word presents
- also since the dataset is divided into 7 separate parts based on the intent, it could be said, that there is annotation of intent of the sentence, but the researchers say that the assistents were train and evaluated on each part independently

### What format is it stored in ?
- `.json`

### What is the license ?
- `Creative commons zero v1.0` therefore the dataset is free even for commercial use, but must be used with trademark and full citation to the original paper presenting the `snips` technology

## MEASUREMENTS:
- since the dataset is divided into 7 parts and researchers say that each part was meant to be used separately, I considered the separate stats of each of 7 parts

#### PlayMusic
- number of sentences: `2000`
- number of words: `14056`
- mean of sentence lengths: `7.028`
- std deviation of sentence lengths: `2.4942365565439153`
- vocabulary size: `3280`
- vocabulary entropy: `5.773917548107008`

#### RateBook
- number of sentences: `1956`
- number of words: `17271`
- mean of sentence lengths: `8.829754601226995`
- std deviation of sentence lengths: `2.968848857547556`
- vocabulary size: `1987`
- vocabulary entropy: `4.801117652064211`

#### SearchScreeningEvent
- number of sentences: `1959`
- number of words: `16972`
- mean of sentence lengths: `8.663603879530372`
- std deviation of sentence lengths: `2.8213177908348746`
- vocabulary size: `1922`
- vocabulary entropy: `5.166915011309278`

#### AddToPlaylist
- number of sentences: `1942`
- number of words: `17663`
- mean of sentence lengths: `9.095262615859939`
- std deviation of sentence lengths: `2.3958615641208234`
- vocabulary size: `3785`
- vocabulary entropy: `5.880047359424887`

#### SearchCreativeWork
- number of sentences: `1954`
- number of words: `14997`
- mean of sentence lengths: `7.675025588536336`
- std deviation of sentence lengths: `2.497146913301756`
- vocabulary size: `3512`
- vocabulary entropy: `5.951883767820372`

#### GetWeather
- number of sentences: `2000`
- number of words: `18569`
- mean of sentence lengths: `9.2845`
- std deviation of sentence lengths: `2.901820075400969`
- vocabulary size: `2637`
- vocabulary entropy: `5.4205911316770985`

#### BookRestaurant
- number of sentences: `1973`
- number of words: `23391`
- mean of sentence lengths: `11.855549923973644`
- std deviation of sentence lengths: `3.6932447290496273`
- vocabulary size: `3087`
- vocabulary entropy: `5.346690643240945`

#### Total stats of the dataset :
- number of sentences: `13784`
- number of words: `122919`
- vocabulary size: `15125`

- formulas used:
- mean: `number of words` $`/`$ `number of sentences`
- std dev: $`\sqrt\frac{\sum (sentence length - mean)^2}{number of sentences}`$
- entropy: $`\sum_i p_i*log(p_i)`$
- probability of `word`: `word count` $`/`$ `number of all words`
